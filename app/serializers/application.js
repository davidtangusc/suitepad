import DS from 'ember-data';
import Ember from 'ember';

export default DS.JSONSerializer.extend({
  normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
    return this._super(store, primaryModelClass, payload.statuses, id, requestType);
  },
  keyForAttribute(key) {
    return Ember.String.underscore(key);
  }
});
