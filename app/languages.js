export default [
  {
    "code": "fr",
    "name": "French"
  },
  {
    "code": "en",
    "name": "English"
  },
  {
    "code": "ar",
    "name": "Arabic"
  },
  {
    "code": "ja",
    "name": "Japanese"
  },
  {
    "code": "es",
    "name": "Spanish"
  },
  {
    "code": "de",
    "name": "German"
  },
  {
    "code": "it",
    "name": "Italian"
  },
  {
    "code": "id",
    "name": "Indonesian"
  },
  {
    "code": "pt",
    "name": "Portuguese"
  },
  {
    "code": "ko",
    "name": "Korean"
  },
  {
    "code": "tr",
    "name": "Turkish"
  },
  {
    "code": "ru",
    "name": "Russian"
  },
  {
    "code": "nl",
    "name": "Dutch"
  },
  {
    "code": "fil",
    "name": "Filipino"
  },
  {
    "code": "msa",
    "name": "Malay"
  },
  {
    "code": "zh-tw",
    "name": "Traditional Chinese"
  },
  {
    "code": "zh-cn",
    "name": "Simplified Chinese"
  },
  {
    "code": "hi",
    "name": "Hindi"
  },
  {
    "code": "no",
    "name": "Norwegian"
  },
  {
    "code": "sv",
    "name": "Swedish"
  },
  {
    "code": "fi",
    "name": "Finnish"
  },
  {
    "code": "da",
    "name": "Danish"
  },
  {
    "code": "pl",
    "name": "Polish"
  },
  {
    "code": "hu",
    "name": "Hungarian"
  },
  {
    "code": "fa",
    "name": "Farsi"
  },
  {
    "code": "he",
    "name": "Hebrew"
  },
  {
    "code": "ur",
    "name": "Urdu"
  },
  {
    "code": "th",
    "name": "Thai"
  },
  {
    "code": "en-gb",
    "name": "English UK"
  }
];
