import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  namespace: '1.1/search',
  pathForType() {
    return 'tweets.json';
  }
});
