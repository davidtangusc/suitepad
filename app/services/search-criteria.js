import Ember from 'ember';
import languages from 'suitepad/languages';

export default Ember.Service.extend({
  init() {
    this._super(...arguments);
    let criteria = localStorage.getItem('criteria');

    if (!criteria) {
      return;
    }

    criteria = JSON.parse(criteria);
    this.set('text', criteria.text);
    this.set('geocode', criteria.geocode);

    if (criteria.selectedLanguage) {
      let language = languages.find((language) => {
        return criteria.selectedLanguage.code === language.code;
      });
      this.set('selectedLanguage', language);
    }
  },
  save() {
    let criteria = this.getProperties(['text', 'selectedLanguage', 'geocode']);
    localStorage.setItem('criteria', JSON.stringify(criteria));
  },
  prepareForRequest() {
    if (!this.get('text') && !this.get('selectedLanguage.code') && !this.get('geocode')) {
      return false;
    }

    return {
      q: this.get('text'),
      lang: this.get('selectedLanguage.code'),
      geocode: this.get('geocode')
    };
  }
});
