import Ember from 'ember';
import languages from 'suitepad/languages';

let { Controller, inject, computed } = Ember;

export default Controller.extend({
  languages: languages,
  searchCriteria: inject.service(),
  noPreviousSearch: computed(function() {
    return this.get('searchCriteria').prepareForRequest() === false;
  }),
  actions: {
    languageSelected(language) {
      this.set('searchCriteria.selectedLanguage', language);
    },
    search() {
      this.get('searchCriteria').save();
      let query = this.get('searchCriteria').prepareForRequest();

      if (!query) {
        this.set('formIsInvalid', true);
        return;
      }

      this.set('loading', true);
      this.store.query('tweet', query).then((tweets) => {
        this.set('noPreviousSearch', false);
        this.set('model', tweets);
        this.set('loading', false);
        this.set('formIsInvalid', false);
      });
    }
  }
});
