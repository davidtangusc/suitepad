import Ember from 'ember';

let { Route, inject } = Ember;

export default Route.extend({
  searchCriteria: inject.service(),
  model() {
    let query = this.get('searchCriteria').prepareForRequest();
    if (query) {
      return this.store.query('tweet', query);
    }
  }
});
