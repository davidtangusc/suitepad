import DS from 'ember-data';

let { Model, attr, belongsTo } = DS;

export default Model.extend({
  text: attr('string'),
  user: belongsTo('user', { async: false }),
  createdAt: attr('date'),
  retweetCount: attr('number'),
  favoriteCount: attr('number')
});
