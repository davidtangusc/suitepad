import DS from 'ember-data';

let { Model, attr } = DS;

export default Model.extend({
  name: attr('string'),
  screenName: attr('string'),
  profileImageUrl: attr('string')
});
