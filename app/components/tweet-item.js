import Ember from 'ember';

const TweetComponent = Ember.Component.extend({
  classNames: ['tweet', 'clearfix']
});

TweetComponent.reopenClass({
  positionalParams: ['tweet']
});

export default TweetComponent;
