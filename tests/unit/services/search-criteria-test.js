import { moduleFor, test } from 'ember-qunit';
import languages from 'suitepad/languages';

moduleFor('service:search-criteria', 'Unit | Service | search criteria', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
  afterEach() {
    localStorage.removeItem('criteria');
  }
});

test('if there is no criteria in local storage, nothing will be set on the service', function(assert) {
  let searchCriteria = this.subject();
  assert.equal(searchCriteria.get('text'), null);
  assert.equal(searchCriteria.get('selectedLanguage'), null);
  assert.equal(searchCriteria.get('geocode'), null);
});

test('the search criteria is set on the service if it is stored in local storage', function(assert) {
  localStorage.setItem('criteria', JSON.stringify({
    text: 'emberjs',
    selectedLanguage: {
      code: 'en',
      name: 'English'
    },
    geocode: '37.781157,-122.398720,1mi'
  }));
  let searchCriteria = this.subject();
  assert.equal(searchCriteria.get('text'), 'emberjs');
  assert.strictEqual(searchCriteria.get('selectedLanguage'), languages[1]);
  assert.equal(searchCriteria.get('geocode'), '37.781157,-122.398720,1mi');
});

test('save writes the properties to local storage', function(assert) {
  let searchCriteria = this.subject({
    text: 'emberjs',
    selectedLanguage: {
      code: 'en',
      name: 'English'
    },
    geocode: '37.781157,-122.398720,1mi'
  });
  searchCriteria.save();
  assert.deepEqual(JSON.parse(localStorage.getItem('criteria')), {
    text: 'emberjs',
    selectedLanguage: {
      code: 'en',
      name: 'English'
    },
    geocode: '37.781157,-122.398720,1mi'
  });
});

test('prepareForRequest() returns false if there is no search criteria', function(assert) {
  let searchCriteria = this.subject();
  assert.equal(searchCriteria.prepareForRequest(), false);
});

test('prepareForRequest() returns the query string data for the twitter api', function(assert) {
  let searchCriteria = this.subject({
    text: 'emberjs',
    selectedLanguage: {
      code: 'en',
      name: 'English'
    },
    geocode: '37.781157,-122.398720,1mi'
  });
  assert.deepEqual(searchCriteria.prepareForRequest(), {
    q: 'emberjs',
    lang: 'en',
    geocode: '37.781157,-122.398720,1mi'
  });
});
