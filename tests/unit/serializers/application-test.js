import { moduleFor, test } from 'ember-qunit';

moduleFor('serializer:application', 'Unit | Serializer | application', {
  // Specify the other units that are required for this test.
  needs: []
});

// Replace this with your real tests.
test('keyForAttribute() returns underscored keys', function(assert) {
  let serializer = this.subject();
  assert.equal(serializer.keyForAttribute('retweetCount'), 'retweet_count');
});
