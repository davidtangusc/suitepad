import {
  create,
  visitable,
  collection,
  fillable,
  clickable,
  contains,
  text
} from 'ember-cli-page-object';

export default create({
  visit: visitable('/'),
  fillInSearchInput: fillable('#search-text'),
  clickSearch: clickable('#search-button'),
  containsError: contains('.text-danger'),
  tweets: collection({
    itemScope: '.tweet',
    item: {
      name: text('.name'),
      screenName: text('.screen-name'),
      retweets: text('.retweets'),
      favorites: text('.favorites')
    }
  }),
  noResults: text('.no-results')
});
