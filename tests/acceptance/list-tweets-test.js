import { test } from 'qunit';
import moduleForAcceptance from 'suitepad/tests/helpers/module-for-acceptance';
import page from 'suitepad/tests/pages/index';

moduleForAcceptance('Acceptance | list tweets', {
  afterEach() {
    window.server.shutdown();
    localStorage.removeItem('criteria');
  }
});

test('the page renders no tweets', function(assert) {
  page.visit('/');
  andThen(() => {
    assert.equal(page.tweets().count, 0);
  });
});

test('filling in the form and pressing search renders tweets', function(assert) {
  page
    .visit('/')
    .fillInSearchInput('emberjs')
    .clickSearch();

  andThen(() => {
    assert.equal(page.tweets().count, 15);
    assert.equal(page.tweets(0).name, 'Lobo del alba');
    assert.equal(page.tweets(0).screenName, '@mansrz_');
    assert.equal(page.tweets(0).retweets, '0');
    assert.equal(page.tweets(0).favorites, '0');
  });
});

test('filling in the form and pressing search renders no tweets when there are no tweets', function(assert) {
  page
    .visit('/')
    .fillInSearchInput('asdf')
    .clickSearch();

  andThen(() => {
    assert.equal(page.noResults, 'Your search criteria returned no results. Try another search.');
  });
});

test('pressing search without filling out the form shows an error to the user', function(assert) {
  page
    .visit('/')
    .clickSearch();

  andThen(() => {
    assert.ok(page.containsError('Make sure to fill out at least one form field.'));
    assert.equal(page.tweets().count, 0);
  });
});

test('the page should load tweets if there is search criteria stored in local storage', function(assert) {
  localStorage.setItem('criteria', JSON.stringify({
    text: 'emberjs',
    selectedLanguage: {
      code: 'en',
      status: 'production',
      name: 'English'
    }
  }));

  page.visit('/');

  andThen(() => {
    assert.equal(page.tweets().count, 15);
    assert.equal(page.tweets(0).name, 'Lobo del alba');
    assert.equal(page.tweets(0).screenName, '@mansrz_');
    assert.equal(page.tweets(0).retweets, '0');
    assert.equal(page.tweets(0).favorites, '0');
  });
});

test('the user is notified to make a search on load', function(assert) {
  page.visit('/');

  andThen(() => {
    assert.equal(page.noResults, 'Please make a search.');
  });
});

test('the user is notified that there are no results for their search on load', function(assert) {
  localStorage.setItem('criteria', JSON.stringify({
    text: 'asdf'
  }));

  page.visit('/');

  andThen(() => {
    assert.equal(page.noResults, 'Your search criteria returned no results. Try another search.');
  });
});
