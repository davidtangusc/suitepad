# Suitepad

I built this app with an emphasis on being run in an iPad since I imagine that is similar to a SuitePad. When you view it, use the iPad emulator in Chrome for the best experience.

* Demo URL: [http://suitepad.surge.sh](http://suitepad.surge.sh)
* Repo: https://bitbucket.org/skaterdav85/suitepad

All tests are passing. Run `ember test -s`.

### Testing

There are unit tests in the `tests/unit` folder.

There are also acceptance tests with Ember CLI Page Object and Ember CLI Mirage. See the following files:

* `tests/acceptance/list-tweets-test.js`
* `tests/pages/index.js`
