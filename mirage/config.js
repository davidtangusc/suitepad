import tweetsJSONResponse from 'suitepad/mirage/data/tweets';

export default function() {
  this.urlPrefix = 'http://demo.suitepad.systems';
  this.namespace = '1.1/search';
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  this.get('/tweets.json', function(schema, request) {
    if (request.queryParams.q === 'asdf') {
      return {
        statuses: []
      };
    }

    return tweetsJSONResponse;
  });
}
